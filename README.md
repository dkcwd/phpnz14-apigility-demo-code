# README #

This repo contains a simple project I created to show some basic features of Apigility at PHP New Zealand 2014.
If you would find additional commentary useful then please let me know. When the video of the presentation is available I will link to it here.

### This repository provides ###

* a quick overview of a Apigility Code Connected REST resource
* an export of the database I used during the demonstration
* the Postman (packaged Chrome app) collection I used for the demo
* the slides I used for the presentation

Note: see the files within project_path/apigility/module/PropertyApp/src/PropertyApp/V1/Rest

The Agent folder is an example of the default skeleton files created when you use the Apigility admin UI to build a new resource. 

The Property folder is an example of files which have been modified to allow GET PUT POST and PATCH 
 
### How do I get set up? ###

You need to be using PHP version 5.4 or above

* clone this repo
* cd to the apigility folder
* run composer install
* run `php -S localhost:8888 -t public public/index.php`
* make sure you have MySQL running
* import the database
* update the database configuration with your credentials
* install Postman (packaged Chrome app)
* import the postman collection which contains the example requests

Note: to configure the database connection update the credentials in project_path/apigility/module/Application/Module.php

### Links

* http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
* http://stateless.co/hal_specification.html
* http://www.zimuel.it/oauth2-apigility/
* http://www.zimuel.it/documenting-apis-using-apigility/

### Contribution guidelines ###

I'm not expecting any contributions however please feel free to raise any questions related to the project/presentation.

Anything which is specific to Apigility itself will be better directed to the relevant channel  

### Who do I talk to? ###

* twitter `@dkcwd`
* Check the google group group details at apigility.org:
* users - https://groups.google.com/a/zend.com/forum/#!forum/apigility-users
* contributors - https://groups.google.com/a/zend.com/forum/#!forum/apigility-dev
