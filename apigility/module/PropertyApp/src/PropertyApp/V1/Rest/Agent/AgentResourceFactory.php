<?php
namespace PropertyApp\V1\Rest\Agent;

class AgentResourceFactory
{
    public function __invoke($services)
    {
        return new AgentResource();
    }
}
