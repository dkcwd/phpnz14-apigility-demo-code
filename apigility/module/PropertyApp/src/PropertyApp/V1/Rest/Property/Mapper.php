<?php

namespace PropertyApp\V1\Rest\Property;

use PropertyApp\V1\Rest\Property\Collection;
use PropertyApp\V1\Rest\Property\Entity;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbTableGateway;
use Zend\Paginator\Paginator;
use Traversable;
use Zend\Stdlib\ArrayUtils;
use Zend\Stdlib\Hydrator\ClassMethods;
use ZF\ApiProblem\Exception\DomainException;
use Rhumsaa\Uuid\Uuid;

class Mapper
{
    /**
     * @var TableGateway
     */
    protected $table;

    /**
     * @param TableGateway $table
     */
    public function __construct(TableGateway $table)
    {
        $this->table = $table;
    }

    /**
     * @param array|Traversable|\stdClass $data
     * @return Entity
     */
    public function create($data)
    {
        if ($data instanceof Traversable) {
            $data = ArrayUtils::iteratorToArray($data);
        }

        if (is_object($data)) {
            $data = (array) $data;
        }

        if (!is_array($data)) {
            throw new \InvalidArgumentException(sprintf(
                'Invalid data provided to %s; must be an array or Traversable',
                __METHOD__
            ));
        }

        /** @var Uuid $id */
        $id = Uuid::uuid4();
        $data['id'] = $id->toString();
        $this->table->insert($data);

        $resultSet = $this->table->select(
            array(
                'id' => $data['id']
            )
        );

        if (0 === count($resultSet)) {
            throw new DomainException('Insert operation failed or did not result in new row', 500);
        }

        return $resultSet->current();
    }

    /**
     * @param string $id
     * @throws DomainException
     * @return Entity
     */
    public function fetch($id)
    {
        $this->validateUuid($id);

        $resultSet = $this->table->select(
            array(
                'id' => $id
            )
        );

        if (0 === count($resultSet)) {
            throw new DomainException('Not found', 404);
        }

        return $resultSet->current();
    }

    /**
     * @return Collection
     */
    public function fetchAll()
    {
        return new Collection(new DbTableGateway($this->table));
    }

    /**
     * @param $id
     * @param $data
     *
     * @return Entity
     */
    public function put($id, $data)
    {
        return $this->patch($id, $data);
    }

    /**
     * @param $id
     * @param $data
     *
     * @return Entity
     */
    public function patch($id, $data)
    {
        $this->validateUuid($id);
        $data = (array) $data;

        /**
         * Prevent update to the uuid
         */
        if (isset($data['id'])) {
            unset($data['id']);
        }

        $where = new Where();
        $where->expression('id = ?', $id);
        $this->table->update($data, $where);
        return $this->fetch($id);
    }

    /**
     * @param $uuid
     * @throws \ZF\ApiProblem\Exception\DomainException
     */
    protected function validateUuid($uuid)
    {
        if (! Uuid::isValid($uuid)) {
            throw new DomainException(
                'The unique identifier is not a valid uuid format',
                422
            );
        }
    }
}
