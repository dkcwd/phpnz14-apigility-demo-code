<?php

namespace PropertyApp\V1\Rest\Property;

class Entity
{
    protected $id;
    protected $formatted_property_address;
    protected $property_type;
    protected $lat;
    protected $lng;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $formatted_property_address
     */
    public function setFormattedPropertyAddress($formatted_property_address)
    {
        $this->formatted_property_address = $formatted_property_address;
    }

    /**
     * @return mixed
     */
    public function getFormattedPropertyAddress()
    {
        return $this->formatted_property_address;
    }

    /**
     * @param mixed $property_type
     */
    public function setPropertyType($property_type)
    {
        $this->property_type = $property_type;
    }

    /**
     * @return mixed
     */
    public function getPropertyType()
    {
        return $this->property_type;
    }

    /**
     * @param mixed $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return mixed
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param mixed $lng
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
    }

    /**
     * @return mixed
     */
    public function getLng()
    {
        return $this->lng;
    }

}
