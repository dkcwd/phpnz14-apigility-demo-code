<?php

namespace PropertyApp\V1\Rest\Property;

use Zend\ServiceManager\ServiceLocatorInterface;
use ZF\ApiProblem\Exception\DomainException;

class MapperFactory
{
    /**
     * @param ServiceLocatorInterface $services
     * @return Mapper
     * @throws \ZF\ApiProblem\Exception\DomainException
     */
    public function __invoke(ServiceLocatorInterface $services)
    {
        $serviceName = 'PropertyTableGateway';
        if (! $services->has($serviceName)) {
            throw new DomainException(
                sprintf(
                    'Cannot create %s; missing TableGateway dependency',
                    $serviceName
                )
            );
        }
        return new Mapper($services->get($serviceName));
    }
}