<?php

namespace PropertyApp\V1\Rest\Property;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\TableGateway as ZFTableGateway;
use Zend\Stdlib\Hydrator\ClassMethods as EntityHydrator;

class TableGateway extends ZFTableGateway
{
    public function __construct($table, AdapterInterface $adapter, $features = null)
    {
        $resultSet = new HydratingResultSet(new EntityHydrator(), new Entity());
        return parent::__construct($table, $adapter, $features, $resultSet);
    }
}